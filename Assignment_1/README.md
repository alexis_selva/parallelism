"Hello world" from cluster

This introductory lab is designed to get you familiar with the assignment submission process.

You will be working on compiling a "hello world" program, "main.cc", that prints not only "hello world" but also whether the application was compiled with the GNU compiler or Intel Compiler. We use a couple of compiler macros to achieve this:

#ifdef __INTEL_COMPILER
  // Only compiled with Intel Compiler
  printf("Hello world from Intel compiler");
#elif __GNUC__
  // Only compiled with GNU Compiler
  printf("Hello world from GNU compiler");
#endif
  
The included "main.cc" file does not need any modification. Instead, you will work on an incomplete "Makefile" file that is included. The compilation line for GNU compiler is there but the line for Intel C++ Compiler is missing. Complete the Makefile by adding the appropriate compilation line using the Intel C++ compiler, "icpc". (hint: it is only one word different from the GNU line)