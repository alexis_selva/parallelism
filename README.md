These are the programming assignments relative to Fundamentals of Parallelism on Intel Architecture:

- Assignment 1: "Hello world" from cluster
- Assignment 2: Vectorizing Monte-Carlo Diffusion
- Assignment 3: Multithreaded Filtering
- Assignment 4: Batch FFTs in HBM
- Assignment 5: MPI String Vibration

For more information, I invite you to have a look at https://www.coursera.org/learn/parallelism-ia
