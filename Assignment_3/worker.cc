#include <vector>
#include <algorithm>
#include <omp.h>

//helper function , refer instructions on the lab page
void append_vec(std::vector<long> &v1, std::vector<long> &v2) {
  v1.insert(v1.end(),v2.begin(),v2.end());
}


void filter(const long n, const long m, float *data, const float threshold, std::vector<long> &result_row_ind) {
   const long nPrime = n - n % 1024;

   //work on one row at a time
#pragma omp parallel for
   for (long ii = 0; ii < nPrime; ii+=1024){
      std::vector<long> result_row_ind_thr;
      for (long i = ii; i < ii + 1024; i++) { 
         
	 //compute sum of all the elements in a row
	 float sum = 0.0f;
	 for (long j = 0; j < m; j++) {
	    sum += data[i * m + j];
	 }

         //store the sum in an array(vector) only if it is valid (i.e if it is greater than threshold
         if (sum > threshold){
            result_row_ind_thr.push_back(i);
	 }
      }

#pragma omp critical 
      {
         append_vec(result_row_ind, result_row_ind_thr);
      }
   }

   //sort the values stored in the vector
   std::sort(result_row_ind.begin(),
             result_row_ind.end());
}
