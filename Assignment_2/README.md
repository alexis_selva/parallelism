Vectorizing Monte-Carlo Diffusion

In this lab, you will be working with an application simulating 1-D random walk. We have a large number of particles starting at the center (x=0), and at each time step the particles are allowed to move randomly according to a given distribution function. The goal is to find the number of particles that have position (x) greater than a certain threshold

Distribution function can be found in "distribution.cc" and "distribution.h".

 const float delta_max = 1.0f;
float dist_func(const float alpha, const float rn) {
  return delta_max*sinf(alpha*rn)*expf(-rn*rn);
}
Here, rn is a randomly generated number

The distribution function is used in simulating diffusion in the "diffusion.cc" file.

int diffusion(
     const int n_particles,     // num of particles
     const int n_steps,         // num of timesteps
     const float x_threshold,   // x cutoff
     const float alpha,         // for dist_func
     VSLStreamStatePtr rnStream // RNG
    ) {
  int n_escaped=0;
  for (int i = 0; i < n_particles; i++) {
    float x = 0.0f;
    for (int j = 0; j < n_steps; j++) {
      float rn;
      // Intel(R) MKL RNG
      vsRngUniform(VSL_RNG_METHOD_UNIFORM_STD,
                   rnStream, 1, &rn, -1.0, 1.0);
      x += dist_func(alpha, rn); 
    }
    if (x > x_threshold) n_escaped++;
  }
  return n_escaped; 
}
Unfortunately, the diffusion workload starts unvectorized. Modify "distribution.cc", "distribution.h" and "diffusion.cc" so that auto-vectorizer can vectorize diffusion().

Hints:
We recommend approcahing this assignment in the following two steps.

Step 1: vectorizing dist_func()
Add either a c++ attribute or OpenMP pragma to instruct the compiler produce a vectorized version of the function.

Step 2: vectorizing diffusion()
The diffusion workload has a true vector dependency over timesteps, so this workload can not be vectorized over timesteps. One solution to this issue is to implement loop-interchange to make the particle loop to the inner loop.

In order to implement the interchange, you must create a temporary buffer to store the positions of the particles. Furthermore, the random number generator can't be in the vectorized loop. So you must generate and store multiple random numbers before the vectorized loop. You can generate n_particles random numbers with:

float rn[n_particles];
vsRngUniform(VSL_RNG_METHOD_UNIFORM_STD,
             rnStream, n_particles, rn, -1.0, 1.0);
You need to add an appropiate pragma to have the compiler use vectorized version of dist_func()

Rules:
There are few rules for this lab:
Do NOT use explicit vectorization for this assignment. The grading script specifically checks for auto vectorization.
Do not add multi-threading for this assignment. The grading script limits the application to a single core.
Do not change the name of the distribution function "dist_func()". The grading script specifically looks for that function name.
Running app:
The grading script uses the following command to run the application.
% taskset -c 0 ./app $ALPHA $THRESHOLD 